configfile: "config/config.yaml"


BATCHES, = glob_wildcards("data/batches/{id}/pmids.txt")

rule all:
	input:
		pmcids = expand("data/batches/{B}/pmcids.txt", B = BATCHES),
		pmcids_full = "data/batches/pmcids.full.txt",
		pmcids_fld = expand("data/batches/corpus/{B}/", B = BATCHES) 



'''
https://www.ebi.ac.uk/europepmc/webservices/rest/search?query=(EXT_ID:4581137%20OR%20EXT_ID:
15115801)%20AND%20SRC:MED%20AND%20OPEN_ACCESS:y 
'''
rule get_PMCID:
    input:
        pmid_file = "data/batches/{B}/pmids.txt"
    output:
        pmcid_file = "data/batches/{B}/pmcids.txt"
    params:
        request_root = config["PMC_REQUEST_ROOT"]
    run:
        import io
        import os
        import requests
        from requests.structures import CaseInsensitiveDict
        from requests.utils import requote_uri
        import pandas
        import json
        df = pandas.read_csv(input.pmid_file, names = ['pmid'])
        RQT_OPS = '%20OR%20EXT_ID:'.join(list(map(str, df.pmid)))
        request = params.request_root + "search?query=(" + RQT_OPS + ")"
        request = request + "%20AND%20SRC:MED%20AND%20OPEN_ACCESS:y&resultType=idlist"
        headers = CaseInsensitiveDict()
        headers["accept"] = "application/json"
        resp = requests.get(request, headers=headers)
        binary = resp.content
        Jdata = json.loads(binary)
        Jlist  = Jdata["resultList"]["result"]
        pmcids = [e['pmcid'] for e in Jlist if 'pmcid' in e.keys()]
        pandas.DataFrame(pmcids).to_csv(output.pmcid_file, index=False)
        
        
'''
'''
rule merge:
    input:
        pmcid_file = expand("data/batches/{B}/pmcids.txt", B=BATCHES)
    output:
        pmcid_file = "data/batches/pmcids.full.txt"
    run:
        with open(output.pmcid_file, 'w') as out:
            for fname in input.pmcid_file:
                with open(fname) as infile:
                    out.write(infile.read())



rule get_XML:
    input:
        pmcid_file = "data/batches/{B}/pmcids.txt"
    output:
        corpus_folder = directory("data/batches/corpus/{B}/")
    params:
        request_root = config["PMC_REQUEST_ROOT"]
    run:
        import os
        import io
        import requests
        from requests.structures import CaseInsensitiveDict
        from requests.utils import requote_uri
        import pandas
        import json
        df = pandas.read_csv(input.pmcid_file, names = ['pmcid'])
        data = [] 
        for index, row in df.iterrows():
            pmcid = row["pmcid"]
            request = params.request_root
            request = request + pmcid
            request = request + "/fullTextXML"
            headers = CaseInsensitiveDict()
            headers["accept"] = "application/xml"
            resp = requests.get(request, headers=headers)
            filename = output.corpus_folder + '/'+ pmcid + '.xml'
            os.makedirs(os.path.dirname(filename), exist_ok=True)
            with open(filename, 'w+') as f:
                f.write(resp.text)


