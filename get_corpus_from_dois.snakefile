configfile: "config/config.yaml"


rule all:
	input:
		dois = config["PMCID_FILE"],
		corpus = config["CORPUS_FOLDER"]


rule get_PMCID:
    input:
        doi_file = config["DOI_FILE"]
    output:
        pmcid_file = config["PMCID_FILE"],
        doi_folder = directory("data/dois"),
        doi_not_found_file = ("data/not_found_dois.txt")
    params:
        request_root = config["PMC_REQUEST_ROOT"]
    run:
        import io
        import os
        import requests
        from requests.structures import CaseInsensitiveDict
        from requests.utils import requote_uri
        import pandas
        import json
        df = pandas.read_csv(input.doi_file)
        found = []
        not_found = []
        for index, row in df.iterrows():
            doi = row["doi"]
            request = params.request_root + "search?query=DOI%3D"
            request = request + doi
            request = request + "&resultType=lite&cursorMark=*&pageSize=25&fromSearchPost=false"
            headers = CaseInsensitiveDict()
            headers["accept"] = "application/json"
            resp = requests.get(request, headers=headers)
            binary = resp.content
            Jdata = json.loads(binary)
            Jlist  = Jdata["resultList"]["result"]
            if len(Jlist) and "fullTextIdList" in Jlist[0] and "fullTextId" in Jlist[0]["fullTextIdList"] and len(Jlist[0]["fullTextIdList"]["fullTextId"]):
                pmcid = Jlist[0]["fullTextIdList"]["fullTextId"][0]
                found.append(pmcid)
            else:
                not_found.append(doi)
            filename = output.doi_folder + '/'+ str(doi) + '.json'
            os.makedirs(os.path.dirname(filename), exist_ok=True)
            with open(filename, 'w+') as f:
                f.write(resp.text)
        df = pandas.DataFrame(found, columns=['pmcid'])
        df.to_csv(output.pmcid_file, index=False)
        pandas.DataFrame(not_found, columns=['doi']).to_csv(output.doi_not_found_file, index=False)





rule get_XML:
    input:
        doi_file = config["PMCID_FILE"]
    output:
        corpus_folder = directory(config["CORPUS_FOLDER"])
    params:
        request_root = config["PMC_REQUEST_ROOT"]
    run:
        import os
        import io
        import requests
        from requests.structures import CaseInsensitiveDict
        from requests.utils import requote_uri
        import pandas
        import json
        df = pandas.read_csv(input.doi_file)
        data = [] 
        for index, row in df.iterrows():
            pmcid = row["pmcid"]
            request = params.request_root
            request = request + pmcid
            request = request + "/fullTextXML"
            headers = CaseInsensitiveDict()
            headers["accept"] = "application/xml"
            resp = requests.get(request, headers=headers)
            filename = output.corpus_folder + '/'+ pmcid + '.xml'
            os.makedirs(os.path.dirname(filename), exist_ok=True)
            with open(filename, 'w+') as f:
                f.write(resp.text)