configfile: "config/config.yaml"

rule all:
	input:
		"data/jointure.csv"


rule joint:
    input:
        an = "data/open16s.csv",
	id = "data/ID2Bioproject.csv"
    output:
        data = "data/jointure.csv"
    run:
        import csv
        import pandas
	an = pandas.read_csv(input.an, sep=',')
	id = pandas.read_csv(input.id, sep=',')
	an['PMCID']=an['PMCID'].astype(str)
	id['PMCID']=id['PMCID'].astype(str)
	#an['PMC'] = 'PMC'
        #an['PMCID_'] = an["PMCID"].astype(str)
	md = pandas.merge(id, an,  on='PMCID')
	md['base_url'] = "https://cati-boom.pages.mia.inra.fr/open16s/"
	md['html'] = ".html"
	md['GitLabPage'] = md['base_url'] + md['PMCID'] + md['html']
	md.drop('base_url', inplace=True, axis=1)
	md.drop('html', inplace=True, axis=1)
	md.to_csv(output.data, index = False, sep = ',', quoting=csv.QUOTE_ALL)
