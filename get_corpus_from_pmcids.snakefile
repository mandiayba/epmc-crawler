configfile: "config/config.yaml"


rule all:
	input:
		corpus = config["CORPUS_FOLDER"]


rule get_XML:
    input:
        doi_file = config["PMCID_FILE"]
    output:
        corpus_folder = directory(config["CORPUS_FOLDER"])
    params:
        request_root = config["PMC_REQUEST_ROOT"]
    run:
        import os
        import io
        import requests
        from requests.structures import CaseInsensitiveDict
        from requests.utils import requote_uri
        import pandas
        import json
        df = pandas.read_csv(input.doi_file)
        data = [] 
        for index, row in df.iterrows():
            pmcid = row["pmcid"]
            request = params.request_root
            request = request + pmcid
            request = request + "/fullTextXML"
            headers = CaseInsensitiveDict()
            headers["accept"] = "application/xml"
            resp = requests.get(request, headers=headers)
            filename = output.corpus_folder + '/'+ pmcid + '.xml'
            os.makedirs(os.path.dirname(filename), exist_ok=True)
            with open(filename, 'w+') as f:
                f.write(resp.text)
