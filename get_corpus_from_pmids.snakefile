configfile: "config/config.yaml"


rule all:
	input:
		pmcids = config["PMCID_FILE"],
		corpus = config["CORPUS_FOLDER"]


rule get_PMCID:
    input:
        pmid_file = config["PMID_FILE"]
    output:
        pmcid_file = config["PMCID_FILE"],
        pmid_folder = directory("data/pmid"),
        pmid_not_found_file = ("data/not_found_pmids.txt")
    params:
        request_root = config["PMC_REQUEST_ROOT"]
    run:
        import io
        import os
        import requests
        from requests.structures import CaseInsensitiveDict
        from requests.utils import requote_uri
        import pandas
        import json
        df = pandas.read_csv(input.pmid_file)
        found = []
        not_found = []
        for index, row in df.iterrows():
            pmid = row["pmid"]
            request = params.request_root + "search?query="
            request = request + str(pmid)
            request = request + "&resultType=lite&cursorMark=*&pageSize=25&fromSearchPost=false"
            headers = CaseInsensitiveDict()
            headers["accept"] = "application/json"
            resp = requests.get(request, headers=headers)
            binary = resp.content
            Jdata = json.loads(binary)
            Jlist  = Jdata["resultList"]["result"]
            if len(Jlist) and "fullTextIdList" in Jlist[0] and "fullTextId" in Jlist[0]["fullTextIdList"] and len(Jlist[0]["fullTextIdList"]["fullTextId"]):
                pmcid = Jlist[0]["fullTextIdList"]["fullTextId"][0]
                found.append(pmcid)
            else:
                not_found.append(pmid)
            filename = output.pmid_folder + '/'+ str(pmid) + '.json'
            os.makedirs(os.path.dirname(filename), exist_ok=True)
            with open(filename, 'w+') as f:
                f.write(resp.text)
        pandas.DataFrame(found, columns=['pmcid']).to_csv(output.pmcid_file, index=False)
        pandas.DataFrame(not_found, columns=['pmid']).to_csv(output.pmid_not_found_file, index=False)



rule get_XML:
    input:
        pmcid_file = config["PMCID_FILE"]
    output:
        corpus_folder = directory(config["CORPUS_FOLDER"])
    params:
        request_root = config["PMC_REQUEST_ROOT"]
    run:
        import os
        import io
        import requests
        from requests.structures import CaseInsensitiveDict
        from requests.utils import requote_uri
        import pandas
        import json
        df = pandas.read_csv(input.pmcid_file)
        data = [] 
        for index, row in df.iterrows():
            pmcid = row["pmcid"]
            request = params.request_root
            request = request + pmcid
            request = request + "/fullTextXML"
            headers = CaseInsensitiveDict()
            headers["accept"] = "application/xml"
            resp = requests.get(request, headers=headers)
            filename = output.corpus_folder + '/'+ pmcid + '.xml'
            os.makedirs(os.path.dirname(filename), exist_ok=True)
            with open(filename, 'w+') as f:
                f.write(resp.text)
